﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MenuController : MonoBehaviour
{
    public PhotonView PV;
    public bool wizardPicked;
    private int currentClass;
    private int teamPicked;
    public GameObject particlesWizard;
    public GameObject particlesRouge;
    public GameObject particlesMarshal;
    public GameObject alchemistGlow;
    public GameObject marshalGlow;
    public GameObject rogueGlow;
    public GameObject marshalDescription;
    public GameObject rogueDescription;
    public GameObject blueTeamUnselected;
    public GameObject blueTeamSelected;
    public GameObject redTeamUnselected;
    public GameObject redTeamSelected;
    public GameObject photonPlayer;

    void Start()
    {
        PV = GetComponent<PhotonView>();
    }

    public void OnClickClassPick(int whichClass)
    {
        if (PlayerInfo.PI != null)
        {
            if (whichClass == 1)
            {
                PV.RPC("RPC_SelectClass", RpcTarget.AllBuffered, whichClass);

                PlayerInfo.PI.mySelectedClass = whichClass;
                PlayerPrefs.SetInt("MyClass", whichClass);

                PlayerInfo.PI.mySelectedCharacter = whichClass;
                PlayerPrefs.SetInt("MyCharacter", whichClass);

                alchemistGlow.SetActive(false);
                marshalGlow.SetActive(false);
                rogueGlow.SetActive(true);

                marshalDescription.SetActive(false);
                rogueDescription.SetActive(true);
            }

            if (whichClass == 2)
            {
                PV.RPC("RPC_SelectClass", RpcTarget.AllBuffered, whichClass);

                PlayerInfo.PI.mySelectedClass = whichClass;
                PlayerPrefs.SetInt("MyClass", whichClass);

                PlayerInfo.PI.mySelectedCharacter = whichClass;
                PlayerPrefs.SetInt("MyCharacter", whichClass);

                alchemistGlow.SetActive(false);
                marshalGlow.SetActive(true);
                rogueGlow.SetActive(false);

                marshalDescription.SetActive(true);
                rogueDescription.SetActive(false);
            }

            if (whichClass == 0 && wizardPicked == false)
            {
                PV.RPC("RPC_SelectClass", RpcTarget.AllBuffered, whichClass);

                PlayerInfo.PI.mySelectedClass = whichClass;
                PlayerPrefs.SetInt("MyClass", whichClass);

                alchemistGlow.SetActive(true);
                marshalGlow.SetActive(false);
                rogueGlow.SetActive(false);
            }
        }
    }

    [PunRPC]
    void RPC_SelectClass(int whichClass)
    {
        if (whichClass == 0)
        {
            wizardPicked = true;
            currentClass = whichClass;
        }

        if (whichClass != 0 && currentClass == 0)
        {
            wizardPicked = false;
            currentClass = whichClass;
        }
    }

    public void TeamClicked(int teamChosen)
    {
        if (teamChosen == 1)
        {
            redTeamSelected.SetActive(false);
            blueTeamSelected.SetActive(true);
        }

        if (teamChosen == 2)
        {
            redTeamSelected.SetActive(true);
            blueTeamSelected.SetActive(false);
        }
        Hashtable hash = new Hashtable();
        hash.Add("team", teamChosen);
        PhotonNetwork.SetPlayerCustomProperties(hash);
    }

    [PunRPC]
    public void RPC_SendLog(int teamPicked)
    {
        Debug.Log("PhotonViewID " + PV.ViewID + ", CustomProperties Team: " + (int)PhotonNetwork.LocalPlayer.CustomProperties["team"]);
    }
}