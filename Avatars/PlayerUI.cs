﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class PlayerUI : MonoBehaviour
{
    public Text HealthText;
    public Text ManaText;
    public Text PlayerScore;
    private IEnumerator coroutine;

    public Image healthBar;
    public Image manaBar;
    public Text totalLives;
    public GameObject respawnText;

    private AvatarSetup avatarSetup;
    private TakeDamage takeDamage;
    private PhotonView PV;
    private GameObject gameManager;

    void Start()
    {
        avatarSetup = GetComponent<AvatarSetup>();
        takeDamage = GetComponent<TakeDamage>();
        PV = GetComponent<PhotonView>();
        gameManager = GameObject.Find("GameManager");
        respawnText = transform.Find("Canvas/RespawnText").gameObject;

        if (PV.IsMine)
        {
            healthBar.transform.parent.gameObject.SetActive(true);
            manaBar.transform.parent.gameObject.SetActive(true);
        }
    }

    void Update()
    {
        healthBar.fillAmount = (float)takeDamage.health / 100f;
        manaBar.fillAmount = (float)avatarSetup.playerMana / 100f;
    }

    public void showRespawnText()
    {
        Debug.Log("showtext caLLed..");
        respawnText.GetComponent<Text>().text = "Respawning in 5 sec Pick location";
        coroutine = disableRespawnText(5.0f);
        StartCoroutine(coroutine);
    }

    private IEnumerator disableRespawnText(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        respawnText.GetComponent<Text>().text = "";
    }
}
