﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AvatarRespawn : MonoBehaviour
{
    private PlayerUI playerUI;
    private Animator animator;
    private PhotonView PV;
    public bool playerIsDead;
    private bool spawnPositionChosen;
    private Vector3 selectedSpawnPosition;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        playerUI = GetComponent<PlayerUI>();
    }

    void Update()
    {
        if (animator == null)
        {
            animator = GetComponentInChildren<Animator>();
            Debug.Log("Animator found.");
        }

        if (!PV.IsMine)
        {
            return;
        }

        if (transform.gameObject.GetComponent<TakeDamage>().health <= 0 && playerIsDead == false || transform.position.y < -10 && playerIsDead == false)
        {
            GetComponent<PlayerUI>().showRespawnText();
            PV.RPC("RPC_PlayerDeath", RpcTarget.All);
        }

        if (playerIsDead == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SelectSpawnPosition();
            }
        }
    }

    [PunRPC]
    void RPC_PlayerDeath()
    {
        playerIsDead = true;
        StartCoroutine("WaitForRespawn");
    }

    private IEnumerator WaitForRespawn()
    {
        yield return new WaitForSeconds(transform.gameObject.GetComponent<AvatarSetup>().respawnTime);

        if (spawnPositionChosen == false)
        {
            selectedSpawnPosition = FindObjectOfType<PhotonPlayer>().pickSpawn().position;
            spawnPositionChosen = false;
        }
        PV.RPC("RPC_RespawnPlayer", RpcTarget.All, selectedSpawnPosition);
    }

    [PunRPC]
    void RPC_RespawnPlayer(Vector3 spawnPosition)
    {
        transform.position = spawnPosition;
        transform.rotation = FindObjectOfType<PhotonPlayer>().pickSpawn().rotation;

        animator.SetBool("isDead", false);
        animator.SetInteger("condition", 0);

        transform.gameObject.GetComponent<TakeDamage>().health = transform.gameObject.GetComponent<AvatarSetup>().playerStartingHealth;
        transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerStartingMana;
        playerIsDead = false;

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    void SelectSpawnPosition()
    {
        RaycastHit hit;
        Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f);
        if (hit.transform.gameObject.tag == "Ground" && hit.point.y > 1f)
        {
            selectedSpawnPosition = hit.point;
            spawnPositionChosen = true;
        }
    }
}
