﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashObject : MonoBehaviour
{
    private float flashTime = 0.3f;
    Material originalMaterial;
    public Material flashMaterial;
    public new Renderer renderer;
    private IEnumerator coroutine;

    void Start()
    {
        if (renderer != null)
        {
            originalMaterial = renderer.material;
        }
    }

    void Update()
    {
        if (renderer == null)
        {
            GetCharacterRenderer();
        }
    }

    void GetCharacterRenderer()
    {
        AvatarSetup avatarSetup = GetComponent<AvatarSetup>();
        int selectedCharacter = avatarSetup.selectedCharacter;

        if (selectedCharacter == 1)
        {
            renderer = transform.Find("RogueSkin(Clone)").transform.Find("Erika_Archer_Meshes/Erika_Archer_Clothes_Mesh").GetComponent<Renderer>();
        }

        if (selectedCharacter == 2)
        {
            renderer = transform.Find("MarshalSkin(Clone)").transform.Find("Dreyar").GetComponent<Renderer>();
        }

        originalMaterial = renderer.material;
    }

    public void Flash()
    {
        renderer.material = flashMaterial;
        coroutine = resetMaterial(flashTime);
        StartCoroutine(coroutine);
    }

    void ResetColor()
    {
        renderer.material = originalMaterial;
        float lerp = Mathf.PingPong(Time.time, flashTime) / flashTime;
        renderer.material.Lerp(flashMaterial, originalMaterial, lerp);
    }

    private IEnumerator resetMaterial(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        renderer.material = originalMaterial;
    }
}