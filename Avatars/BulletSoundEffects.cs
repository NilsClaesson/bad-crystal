﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSoundEffects : MonoBehaviour
{
    public AudioSource source;
    public AudioClip thud1;
    public AudioClip thud2;
    public AudioClip thud3;

    void Start()
    {
        AudioSource[] audioSource = GetComponents<AudioSource>();
        source = audioSource[2];
        thud1 = audioSource[0].clip;
        thud2 = audioSource[1].clip;
        thud3 = audioSource[2].clip;
    }

    public void PlayThud()
    {
        int randomNumber = Random.Range(1, 3);

        if (randomNumber == 1)
        {
            source.PlayOneShot(thud1);
        }
        if (randomNumber == 2)
        {
            source.PlayOneShot(thud2);
        }
        if (randomNumber == 3)
        {
            source.PlayOneShot(thud3);
        }
    }
}