﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class AvatarSetup : MonoBehaviourPunCallbacks
{

    private PhotonView PV;
    public int characterValue;
    public GameObject myCharacter;
    public GameObject photonPlayer;

    public int playerScore;
    public int playerStartingHealth;
    [HideInInspector]
    public int playerHealth;
    public int playerStartingMana;
    [HideInInspector]
    public int playerMana;
    public int playerDamage;
    public int respawnTime;

    public Camera myCamera;
    public AudioListener myAudioListener;

    public Material blueMaterial;
    public Material redMaterial;
    public int playerTeam;
    public int selectedCharacter;
    bool colorSet;

    void Start()
    {

        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            PV.RPC("RPC_AddCharacter", RpcTarget.AllBuffered, PlayerInfo.PI.mySelectedCharacter);
        }
        else
        {
            Destroy(myCamera);
            Destroy(myAudioListener);
        }
    }

    void Update()
    {
        playerTeam = (int)PV.Owner.CustomProperties["team"];
        if (myCharacter != null && colorSet == false)
        {
            SetColor();
        }
    }

    [PunRPC]
    public void RPC_AddCharacter(int whichCharacter)
    {
        myCharacter = Instantiate(PlayerInfo.PI.allCharacters[whichCharacter], transform.position, transform.rotation, transform);
        selectedCharacter = whichCharacter;
    }


    void SetColor()
    {
        if (myCharacter.name == "RogueSkin(Clone)")
        {
            if (playerTeam == 1)
            {
                myCharacter.transform.Find("Erika_Archer_Meshes/Erika_Archer_Clothes_Mesh").GetComponent<Renderer>().material = blueMaterial;
            }
            else if (playerTeam == 2)
            {
                myCharacter.transform.Find("Erika_Archer_Meshes/Erika_Archer_Clothes_Mesh").GetComponent<Renderer>().material = redMaterial;
            }
            colorSet = true;
        }

        if (myCharacter.name == "MarshalSkin(Clone)")
        {
            if (playerTeam == 1)
            {
                myCharacter.transform.Find("Dreyar").GetComponent<Renderer>().material = blueMaterial;
            }
            else if (playerTeam == 2)
            {
                myCharacter.transform.Find("Dreyar").GetComponent<Renderer>().material = redMaterial;
            }
            colorSet = true;
        }
    }
}