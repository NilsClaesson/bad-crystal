﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PhotonView PV;
    private CharacterController myCC;
    private float defaultMovementSpeed = 8f;
    private float movementSpeed;
    public float rotationSpeed;

    private IEnumerator coroutine;
    public Vector3 lookAt;
    private Camera cam;
    private GameManager gameManager;

    void Start()
    {
        movementSpeed = defaultMovementSpeed;
        PV = GetComponent<PhotonView>();
        myCC = GetComponent<CharacterController>();
        cam = FindObjectOfType<Camera>();

        StartCoroutine(RegenerateMana());
    }

    void Update()
    {
        if (!PV.IsMine)
        {
            return;
        }

        if (transform.gameObject.GetComponent<AvatarRespawn>().playerIsDead)
        {
            return;
        }

        lookAt = GetMousePositionInPlaneOfLauncher() - transform.position;
        Quaternion rotation = Quaternion.LookRotation(lookAt, Vector3.up);
        transform.rotation = rotation;
        BasicMovement();
        BasicRotation();
        GroundPlayer();
    }

    void BasicMovement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            myCC.Move(Vector3.forward * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            myCC.Move(-Vector3.right * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            myCC.Move(-Vector3.forward * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            myCC.Move(Vector3.right * Time.deltaTime * movementSpeed);
        }
    }

    [PunRPC]
    void RPC_RestartLevel()
    {
        Debug.Log("Restarting!");
        PhotonNetwork.LoadLevel(1);
    }

    void BasicRotation()
    {
        float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed;
        transform.Rotate(new Vector3(0, mouseX, 0));
    }

    void GroundPlayer()
    {
        if (myCC.isGrounded == false)
        {
            myCC.Move(Vector3.down * 0.2f);
        }
    }

    IEnumerator RegenerateMana()
    {
        while (true)
        {
            transform.gameObject.GetComponent<AvatarSetup>().playerMana = Mathf.Clamp(transform.gameObject.GetComponent<AvatarSetup>().playerMana + 1, -100, 100);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public Vector3 GetMousePositionInPlaneOfLauncher()
    {
        Plane p = new Plane(Vector3.up, transform.position);
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        float d;

        if (p.Raycast(r, out d))
        {
            Vector3 v = r.GetPoint(d);
            return v;
        }
        else throw new UnityException("Mouse position ray not intersecting launcher plane");
    }
}
