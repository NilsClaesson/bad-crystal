﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;

public class TakeDamage : MonoBehaviour
{
    public int startingHealth;
    [HideInInspector]
    public int health;
    public Player lastHitBy;

    void Start()
    {
        health = startingHealth;
    }

    void Update()
    {
        if (health > startingHealth)
        {
            health = startingHealth;
        }
    }
}