﻿using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int owner;
    private BulletSoundEffects bulletSoundEffects;
    public int bulletSpeed;
    public int damage = 50;
    public float lifetime = 3.0f;
    Vector3 velocity;
    [HideInInspector]
    public bool hasCollided;
    public ParticleSystem explosion;
    private GameObject targetHit;
    private IEnumerator coroutine;

    public void Start()
    {
        var capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
        capsuleCollider.isTrigger = true;
        bulletSoundEffects = GetComponent<BulletSoundEffects>();
        coroutine = destroyExplosion(lifetime);
        StartCoroutine(coroutine);
    }

    void Update()
    {
        transform.position += velocity * bulletSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TakeDamage>() != null && other.gameObject.GetInstanceID() != owner)
        {
            other.gameObject.GetComponent<TakeDamage>().health -= damage;
            ParticleSystem impactExplosion = Instantiate(explosion, transform.position, Quaternion.identity);

            if (other.gameObject.GetComponent<FlashObject>() != null)
            {
                other.gameObject.GetComponent<FlashObject>().Flash();
            }

            Destroy(gameObject);
        }
        else if (other.gameObject.tag != "MarshalSword" && other.gameObject.GetInstanceID() != owner)
        {
            ParticleSystem arrowExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    public void InitializeBullet(int Owner, Vector3 originalDirection, float lag)
    {
        transform.rotation = Quaternion.LookRotation(originalDirection);
        velocity = originalDirection;
        owner = Owner;
    }

    private IEnumerator destroyExplosion(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ParticleSystem arrowExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}