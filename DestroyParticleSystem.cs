﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticleSystem : MonoBehaviour
{
    private ParticleSystem ps;

    void Start()
    {
        Destroy(gameObject, 2f);
        ps = GetComponent<ParticleSystem>();
    }
}
