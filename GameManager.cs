﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    private PhotonView PV;
    public GameObject[] players;
    public int[] playerScores = new int[4];
    public GameObject playerAvatar;
    public GameObject wizard;
    public List<GameObject> monsters;
    public List<GameObject> allEnemies;
    public List<GameObject> doors;
    public Text winText;
    public string scores;
    public PlayerUI playerUI;
    private IEnumerator coroutine;
    private int roomNumber = 0;
    string winTextDisplay;

    void Awake()
    {
        monsters = new List<GameObject>();
        allEnemies = new List<GameObject>();
        doors = new List<GameObject>();
    }

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PV = GetComponent<PhotonView>();
        playerUI = playerAvatar.GetComponent<PlayerUI>();
    }

    [PunRPC]
    void RPC_OpenDoor(bool shouldOpen)
    {
        foreach (GameObject d in doors)
        {
            if (d.gameObject.GetComponent<OpenDoor>().doorNumber == roomNumber)
            {
                d.gameObject.GetComponent<OpenDoor>().Open(shouldOpen);
            }
        }
    }

    public void TeamWin(string text)
    {
        winTextDisplay = text;
        StartCoroutine(WaitForRestart());
    }

    IEnumerator WaitForRestart()
    {
        PV.RPC("RPC_ShowText", RpcTarget.All, true, winTextDisplay);
        yield return new WaitForSeconds(5);
        PV.RPC("RPC_ShowText", RpcTarget.All, false, winTextDisplay);
        PV.RPC("RPC_RestartLevel", RpcTarget.All);
    }

    [PunRPC]
    void RPC_ShowText(bool onOff, string text)
    {
        winText.text = text;
        winText.gameObject.SetActive(onOff);
    }

    [PunRPC]
    void RPC_RestartLevel()
    {
        Debug.Log("Starting Game");
        PhotonNetwork.LoadLevel(2);
    }
}