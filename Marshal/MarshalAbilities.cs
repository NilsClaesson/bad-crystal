﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;
using UnityEngine.UI;

public class MarshalAbilities : MonoBehaviour
{
    private PhotonView PV;
    private AvatarSetup avatarSetup;
    private PlayerMovement playerMovement;
    private TakeDamage takeDamage;
    private GameObject gameManager;
    private AudioEffectsMarshal audioEffectsMarshal;
    private CharacterController myCC;
    private Collider playerCollider;
    public GameObject whirlwindCollider;

    public Animator animator;

    public Transform rayOrigin;
    public GameObject bulletPrefab;
    public GameObject healProjectilePrefab;
    public GameObject forcefieldPrefab;

    private int activeSpell = 1;

    private IEnumerator coroutine;
    public float fireRate = 0.0f;
    private float nextFire;
    [HideInInspector]

    private int forcefieldCost = 50;
    private int healCost = 30;

    private int whirlwindCost = 50;
    private bool isDashing;
    private float dashSpeed = 0f;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        avatarSetup = GetComponent<AvatarSetup>();
        playerMovement = GetComponent<PlayerMovement>();
        takeDamage = GetComponent<TakeDamage>();
        audioEffectsMarshal = GetComponent<AudioEffectsMarshal>();
        myCC = GetComponent<CharacterController>();
        playerCollider = GetComponent<Collider>();
        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        if (animator == null)
        {
            animator = GetComponentInChildren<Animator>();
            Debug.Log("Animator found.");
        }

        if (transform.gameObject.GetComponent<AvatarRespawn>().playerIsDead)
        {
            animator.SetInteger("condition", 4);
            animator.SetBool("isDead", true);
            return;
        }

        if (!PV.IsMine)
        {
            return;
        }

        if (isDashing == true)
        {
            myCC.Move(transform.forward * Time.deltaTime * dashSpeed);
        }
        PlayerInput();
    }

    void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            activeSpell = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            activeSpell = 2;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            activeSpell = 3;
        }

        if (Input.GetMouseButtonDown(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            PV.RPC("RPC_MarshalSlash", RpcTarget.All, transform.position, transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (whirlwindCost <= transform.gameObject.GetComponent<AvatarSetup>().playerMana && isDashing == false)
            {
                nextFire = Time.time + fireRate;
                transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - whirlwindCost;
                PV.RPC("RPC_Whirlwind", RpcTarget.All);
            }
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (forcefieldCost <= transform.gameObject.GetComponent<AvatarSetup>().playerMana)
            {
                transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - forcefieldCost;
                PV.RPC("RPC_Forcefield", RpcTarget.All);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (healCost <= transform.gameObject.GetComponent<AvatarSetup>().playerMana)
            {
                transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - healCost;
                PV.RPC("RPC_Heal", RpcTarget.All, transform.position, transform.rotation);
            }
        }
    }


    [PunRPC]
    public void RPC_MarshalSlash(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        animator.SetBool("isAttacking", true);
        animator.SetInteger("condition", 2); //2 = attacking
        audioEffectsMarshal.PlaySwordSwish();

        coroutine = setIdle(0.6f);
        StartCoroutine(coroutine);
    }

    private IEnumerator setIdle(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        animator.SetInteger("condition", 0); //0 = idling
        animator.SetBool("isAttacking", false); //2 = attacking
    }

    [PunRPC]
    public void RPC_Heal(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);

        Vector3 plusY = new Vector3(0, 1, 0);
        Vector3 fireFrom = transform.position + transform.gameObject.GetComponent<PlayerMovement>().lookAt.normalized * 2f + plusY;

        GameObject healProjectile;
        healProjectile = Instantiate(healProjectilePrefab, fireFrom, Quaternion.identity) as GameObject;
        healProjectile.GetComponent<Bullet>().InitializeBullet(transform.gameObject.GetInstanceID(), (rotation * Vector3.forward), Mathf.Abs(lag));
    }

    [PunRPC]
    public void RPC_Forcefield(PhotonMessageInfo info)
    {
        GameObject forcefield;
        forcefield = Instantiate(forcefieldPrefab, transform.position, Quaternion.identity) as GameObject;
    }

    [PunRPC]
    void RPC_Whirlwind()
    {
        audioEffectsMarshal.PlayWhirlwindAttack();

        animator.SetBool("isAttacking", true); //2 = attacking
        animator.SetInteger("condition", 3); //2 = attacking
        whirlwindCollider.GetComponent<ParticleSystem>().Play();
        whirlwindCollider.GetComponent<Collider>().enabled = true;

        isDashing = true;
        dashSpeed = 50f;

        coroutine = disableDash(0.2f);
        StartCoroutine(coroutine);

        coroutine = setIdle(0.6f);
        StartCoroutine(coroutine);
    }

    private IEnumerator disableDash(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        whirlwindCollider.GetComponent<ParticleSystem>().Stop();
        whirlwindCollider.GetComponent<Collider>().enabled = false;
        dashSpeed = 0f;
        isDashing = false;
    }
}