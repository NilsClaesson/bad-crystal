﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Forcefield : MonoBehaviour
{
    private PhotonView PV;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        Destroy(gameObject, 10f);
    }

    void Update()
    {
        if (transform.gameObject.GetComponent<TakeDamage>().health <= 0)
        {
            PV.RPC("RPC_DestroyForcefield", RpcTarget.All, transform.position, transform.rotation);
        }
    }

    [PunRPC]
    public void RPC_DestroyForcefield(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        Destroy(gameObject);
    }
}