﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEffectsMarshal : MonoBehaviour
{
    AudioSource audioSource;

    public AudioClip swordSwish1;
    public AudioClip swordSwish2;
    public AudioClip swordSwish3;
    public AudioClip whirlwindAttack;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySwordSwish()
    {
        int randomNumber = Random.Range(1, 3);

        if (randomNumber == 1)
        {
            audioSource.PlayOneShot(swordSwish1, 0.7F);
        }
        if (randomNumber == 2)
        {
            audioSource.PlayOneShot(swordSwish2, 0.7F);
        }
        if (randomNumber == 3)
        {
            audioSource.PlayOneShot(swordSwish3, 0.7F);
        }
    }

    public void PlayWhirlwindAttack()
    {
        audioSource.PlayOneShot(whirlwindAttack, 0.7F);
    }
}
