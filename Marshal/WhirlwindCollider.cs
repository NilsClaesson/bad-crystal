﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhirlwindCollider : MonoBehaviour
{

    public int damage = 50;
    public void Start()
    {
        var capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
        capsuleCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TakeDamage>() != null && other.gameObject.name != transform.root.name)
        {
            other.gameObject.GetComponent<TakeDamage>().health -= damage;
            if (other.gameObject.GetComponent<FlashObject>() != null)
            {
                other.gameObject.GetComponent<FlashObject>().Flash();
            }
        }
    }
}