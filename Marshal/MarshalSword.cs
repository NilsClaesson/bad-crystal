﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarshalSword : MonoBehaviour
{
    private int damage = 50;
    public bool canDamage = false;
    private IEnumerator coroutine;

    void Start()
    {
        var boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TakeDamage>() != null && other.gameObject.GetInstanceID() != transform.root.gameObject.GetInstanceID() && canDamage == true)
        {
            other.gameObject.GetComponent<TakeDamage>().health -= damage;

            if (other.gameObject.GetComponent<FlashObject>() != null)
            {
                other.gameObject.GetComponent<FlashObject>().Flash();
            }

            canDamage = false;
        }
    }

    private IEnumerator resetCanDamage(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        canDamage = true;
    }
}