﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectMonster : MonoBehaviour
{
    [HideInInspector]
    public int attackMode = 0;
    [HideInInspector]
    public bool isSelected;
}
