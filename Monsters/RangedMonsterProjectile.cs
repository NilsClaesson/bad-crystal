﻿using Photon.Realtime;
using UnityEngine;

public class RangedMonsterProjectile : MonoBehaviour
{
    private int bulletSpeed = 7;
    public int damage;
    [HideInInspector]
    public bool hasCollided;
    public ParticleSystem explosion;

    public void Start()
    {
        Destroy(gameObject, 3.0f);
    }

    public void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }

    public void InitializeBullet(Vector3 originalDirection)
    {
        transform.forward = originalDirection;
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = originalDirection.normalized * bulletSpeed;
    }
}
