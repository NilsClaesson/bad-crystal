﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSetup : MonoBehaviour
{
    public int health;
    public int damage;
    private GameObject gameManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        if (transform.gameObject.GetComponent<MonsterController>() != null)
        {
            gameManager.GetComponent<GameManager>().monsters.Add(transform.gameObject);
            gameManager.GetComponent<GameManager>().allEnemies.Add(transform.gameObject);
        }
        if (transform.gameObject.GetComponent<MonsterController>() == null)
        {
            Debug.Log("MonsterController not found. Check if attached");
        }
    }
}
