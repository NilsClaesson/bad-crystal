﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RangedMonsterCombat : MonoBehaviour
{
    private MonsterSetup monsterSetup;
    private MonsterController monsterController;
    private AvatarSetup avatarSetup;
    private GameObject gameManager;
    public GameObject playerAvatar;
    private Collider monsterCollider;
    public GameObject rangedMonsterProjectile;
    private PhotonView PV;
    public Animator animator;
    private float fireRate = 0.6f;
    private float nextFire;
    public MonsterSpawner monsterSpawner;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        monsterSetup = GetComponent<MonsterSetup>();
        monsterController = GetComponent<MonsterController>();
        monsterCollider = GetComponent<Collider>();
        avatarSetup = playerAvatar.GetComponent<AvatarSetup>();
        animator = GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        monsterController.agent.isStopped = false;

        Vector3 target = transform.position - monsterController.destination;
        if (target.magnitude < 15f && target.magnitude != 0 && monsterController.isAttacking == true)
        {
            animator.SetInteger("condition", 2); //2 = ranged attack
            monsterController.agent.isStopped = true;
            monsterController.agent.velocity = Vector3.zero;

            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate * monsterController.speedTotemAttackSpeed;
                Fire(transform.position, transform.rotation);
            }
        }

        else if (monsterController.agent.velocity.magnitude > 0)
        {
            monsterController.agent.isStopped = false;
            animator.SetInteger("condition", 1); //1 = running
        }
        else
        {
            animator.SetInteger("condition", 0); //0 = idle
        }

        if (transform.gameObject.GetComponent<MonsterSetup>().health <= 0)
        {
            PV.RPC("RPC_DestroyMonster", RpcTarget.All);
        }
    }

    public void Fire(Vector3 position, Quaternion rotation)
    {
        GameObject bullet;

        Vector3 plusY = new Vector3(0, 1, 0);
        Vector3 fireFrom = transform.position + (monsterController.destination - transform.position).normalized * 1.5f + plusY;

        bullet = Instantiate(rangedMonsterProjectile, fireFrom, Quaternion.identity) as GameObject;
        bullet.GetComponent<RangedMonsterProjectile>().InitializeBullet((rotation * Vector3.forward));
    }

    [PunRPC]
    private void RPC_DestroyMonster(PhotonMessageInfo info)
    {
        gameManager.GetComponent<GameManager>().monsters.Remove(transform.gameObject);
        gameManager.GetComponent<GameManager>().allEnemies.Remove(transform.gameObject);

        if (monsterSpawner != null)
        {
            monsterSpawner.monsterAmount--;
        }
        Destroy(gameObject);
    }

    public void SetSpawner(MonsterSpawner MonsterSpawner)
    {
        monsterSpawner = MonsterSpawner;
    }
}
