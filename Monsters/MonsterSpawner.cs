﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MonsterSpawner : MonoBehaviour
{
    private PhotonView PV;
    private MonsterSetup monsterSetup;
    private TakeDamage takeDamage;
    public GameObject meleeMonster;
    public GameObject rangedMonster;
    public GameObject playerAvatar;
    private GameObject gameManager;
    public MonsterSpawner monsterSpawner;

    public float spawnRate = 3f;
    public int monsterCap;
    public int monsterAmount;
    private float nextSpawn;

    public bool spawnMeleeMonster;
    public bool spawnRangedMonster;

    void Start()
    {
        nextSpawn = Time.time + spawnRate;
        PV = GetComponent<PhotonView>();
        monsterSetup = GetComponent<MonsterSetup>();
        takeDamage = GetComponent<TakeDamage>();
        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        if (PhotonNetwork.IsMasterClient == true)
        {
            if (Time.time > nextSpawn && spawnMeleeMonster == true && monsterCap > monsterAmount)
            {
                nextSpawn = Time.time + spawnRate;
                monsterAmount++;
                PV.RPC("RPC_SpawnMeleeMonster", RpcTarget.All, transform.position, transform.rotation);
            }

            if (Time.time > nextSpawn && spawnRangedMonster == true && monsterCap > monsterAmount)
            {
                nextSpawn = Time.time + spawnRate;
                monsterAmount++;
                PV.RPC("RPC_SpawnRangedMonster", RpcTarget.All, transform.position, transform.rotation);
            }

            if (takeDamage.health <= 0)
            {
                PV.RPC("RPC_DestroyMonster", RpcTarget.All);
            }
        }
    }

    [PunRPC]
    private void RPC_DestroyMonster(PhotonMessageInfo info)
    {
        Destroy(gameObject);
    }

    [PunRPC]
    public void RPC_SpawnMeleeMonster(Vector3 position, Quaternion rotation)
    {
        if (PhotonNetwork.IsMasterClient == true)
        {
            GameObject meleeMonster;
            meleeMonster = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "MeleeMonster"), position, rotation);
            meleeMonster.GetComponent<MeleeMonsterCombat>().SetSpawner(monsterSpawner);
        }
    }

    [PunRPC]
    public void RPC_SpawnRangedMonster(Vector3 position, Quaternion rotation)
    {
        if (PhotonNetwork.IsMasterClient == true)
        {
            GameObject rangedMonster;
            rangedMonster = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "RangedMonster"), position, rotation);
            rangedMonster.GetComponent<RangedMonsterCombat>().SetSpawner(monsterSpawner);
        }
    }
}