﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MeleeMonsterCombat : MonoBehaviour
{
    private MonsterSetup monsterSetup;
    private TakeDamage takeDamage;
    private MonsterController monsterController;
    private FlashObject flashObject;
    private AvatarSetup avatarSetup;
    private MonsterSoundEffects monsterSoundEffects;

    private GameObject gameManager;
    public GameObject playerAvatar;
    private Collider monsterCollider;
    public GameObject BulletPrefab;
    private PhotonView PV;
    public Animator animator;
    public MonsterSpawner monsterSpawner;
    [HideInInspector]
    public bool canDamage;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        monsterSetup = GetComponent<MonsterSetup>();
        takeDamage = GetComponent<TakeDamage>();
        monsterController = GetComponent<MonsterController>();
        flashObject = GetComponent<FlashObject>();
        monsterCollider = GetComponent<Collider>();
        avatarSetup = playerAvatar.GetComponent<AvatarSetup>();
        monsterSoundEffects = GetComponent<MonsterSoundEffects>();

        animator = GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        monsterController.agent.isStopped = false;

        Vector3 target = transform.position - monsterController.destination;
        if (target.magnitude < 1.5f && target.magnitude != 0 && monsterController.isAttacking == true)
        {
            animator.SetInteger("condition", 3); //3 = attacking
            monsterController.agent.isStopped = true;
            monsterController.agent.velocity = Vector3.zero;
        }


        else if (monsterController.agent.velocity.magnitude > 0)
        {
            monsterController.agent.isStopped = false;
            animator.SetInteger("condition", 1); //1 = running
        }

        if (target.magnitude < 0.2f)
        {
            animator.SetInteger("condition", 0); //0 = idling
        }

        if (transform.gameObject.GetComponent<TakeDamage>().health <= 0)
        {
            PV.RPC("RPC_DestroyMonster", RpcTarget.All);
        }
    }

    [PunRPC]
    private void RPC_DestroyMonster(PhotonMessageInfo info)
    {
        gameManager.GetComponent<GameManager>().monsters.Remove(transform.gameObject);
        gameManager.GetComponent<GameManager>().allEnemies.Remove(this.gameObject);

        if (monsterSpawner != null)
        {
            monsterSpawner.monsterAmount--;
        }
        Destroy(gameObject);
    }

    public void SetSpawner(MonsterSpawner MonsterSpawner)
    {
        monsterSpawner = MonsterSpawner;
    }

    public void CanDamage()
    {
        canDamage = true;
        monsterSoundEffects.PlaySwordSwish();
    }

    public void CantDamage()
    {
        canDamage = false;
    }
}
