﻿using UnityEngine;
using UnityEngine.AI;

public class MonsterController : MonoBehaviour
{
    public NavMeshAgent agent;
    public Vector3 destination;
    public GameObject[] players;
    public bool isAttacking;
    public bool isSelected;
    public int mode = 0; //0=default 1=hold 2=move 3=attackmove 
    public float speedTotemAttackSpeed = 1;
    public float speedTotemMoveSpeed = 1;
    public float originalMoveSpeed;
    private new Rigidbody rigidbody;
    private Shader shaderNoOutline;
    private Shader shaderOutline;

    void Start()
    {
        destination = transform.position;
        players = GameObject.FindGameObjectsWithTag("Avatar");
        originalMoveSpeed = agent.speed;
        rigidbody = transform.GetComponent<Rigidbody>();

        shaderNoOutline = Shader.Find("Standard");
        shaderOutline = Shader.Find("UniformOutline");
    }

    void Update()
    {
        if (players.Length == 0)
        {
            players = GameObject.FindGameObjectsWithTag("Avatar");
        }

        Vector3 lookAt = destination - transform.position;

        if (lookAt != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(lookAt, Vector3.up);
            transform.rotation = rotation;
        }

        if ((transform.position - destination).magnitude < 0.2f)
        {
            agent.velocity = Vector3.zero;
            rigidbody.velocity = Vector3.zero;
        }

        if (mode == 0)
        {
            isAttacking = true;
            destination = GetClosestEnemy(players).transform.position;
            agent.SetDestination(destination);
        }

        if (mode == 1)
        {
            isAttacking = true;
            agent.velocity = Vector3.zero;
            rigidbody.velocity = Vector3.zero;
            destination = transform.position;
            agent.SetDestination(destination);
        }
        if (mode == 2)
        {
            isAttacking = false;
            agent.SetDestination(destination);
            Vector3 target = transform.position - destination;
            if (target.magnitude < 0.7f)
            {
                agent.ResetPath();
                mode = 0;
                destination = GetClosestEnemy(players).transform.position;
                agent.SetDestination(destination);
            }
        }
        if (mode == 3)
        {
            agent.SetDestination(destination);
            destination = GetClosestEnemy(players).transform.position;
            Vector3 target = transform.position - destination;
            if (target.magnitude < 20)
            {
                destination = target;
                agent.SetDestination(destination);
            }
        }

        if (isSelected == true)
        {
            transform.GetChild(1).GetComponent<Renderer>().material.shader = shaderOutline;
        }
        else
        {
            transform.GetChild(1).GetComponent<Renderer>().material.shader = shaderNoOutline;
        }
    }

    Transform GetClosestEnemy(GameObject[] players)
    {
        Transform bestTarget = transform;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach (GameObject potentialTarget in players)
        {
            if (potentialTarget.GetComponent<PlayerMovement>().enabled == true)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget.transform;
                }
            }
            else
            {
                bestTarget = transform;
            }
        }

        return bestTarget;
    }
}
