﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeMonsterAttack : MonoBehaviour
{
    private MonsterSetup monsterSetup;
    private MeleeMonsterCombat meleeMonsterCombat;
    public GameObject meleeMonster;

    void Start()
    {
        monsterSetup = meleeMonster.GetComponent<MonsterSetup>();
        meleeMonsterCombat = meleeMonster.GetComponent<MeleeMonsterCombat>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TakeDamage>() != null && other.gameObject.tag == "Avatar" && meleeMonsterCombat.canDamage == true)
        {
            other.gameObject.GetComponent<TakeDamage>().health -= monsterSetup.damage;
            meleeMonsterCombat.canDamage = false;
        }
    }
}
