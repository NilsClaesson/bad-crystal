﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerScores : MonoBehaviour
{
    public const string PlayerScoreProp = "score";

    public void SetScore(Player player, int newScore)
    {
        Hashtable score = new Hashtable();
        score[PlayerScoreProp] = newScore;
        player.SetCustomProperties(score);
    }

    public void AddScore(Player player, int scoreToAddToCurrent)
    {
        int current = GetScore(player);
        current = current + scoreToAddToCurrent;
        Hashtable score = new Hashtable();
        score[PlayerScoreProp] = current;
        player.SetCustomProperties(score);
    }

    public int GetScore(Player player)
    {
        object score;
        if (player.CustomProperties.TryGetValue(PlayerScoreProp, out score))
        {
            return (int)score;
        }

        return 0;
    }
}