# Bad Crystal
Bad Crystal is a fast-paced multiplayer game where players fight in teams over the control of a mysterious crystal. It is inspired by games such as Hammerwatch and DOTA and values player skill and teamwork. The game is set in a sci-fi setting which will be fleshed out in future updates.  Most assets apart from animations have been made by me, buying assets would save time but would be much less fun!

### Demo
Here is gameplay footage from a match between four players: [youtube](https://www.youtube.com/watch?v=Etk4U_d9fqY)

### Technology
The project is built in Unity with C# scripting. Not included in repository are all the other assets such as 3d-models, animations, shaders and sounds. Multiplayer functionality is achieved with [Photon Pun](https://www.photonengine.com/en/PUN). In Pun, clients connect to a cloud-based server which makes for a smoother experience in the case of latency problems.

### Game Objective
The goal of the game is to control the crystal and bring it to your starting position. If you get within range of the crystal (and manages to stay alive!) it binds to you and starts to pathfind towards your base. Once it reaches its destination a point is scored and the match resets, the first team to get three points wins. 

### Playable Classes
The characters share most scripts such as UI, movement, respawn, health and mana management. Character specific scripts include sound, animations and abilities. This makes adding new characters much easier. Bad Crystal in its current incarnation features two distinct classes:

##### **Gemini** 
 - Prefers to fight from a distance
- Ranged attack
- Splitting shot - Projectile which explodes into shrapnel when triggered
- Fog - Creates a thick smoke to hide your position from other players
- Jetpack - For quick escapes

##### **Runciter** 
- Close-combat and support
- Melee attack
- Dash attack
- Forcefield - Has a set amount of health and stops incoming attacks
- Heal projectile

### Future Features
- See list of available games instead of automatically joining
- Proper game lobbys
- Variety of maps supporting 4+ players
- RPG elements such as item drops and leveling
- New class: Alchemist

### Authors
- **Nils Claesson** - _Initial work_ - [Profile](https://gitlab.com/NilsClaesson)


