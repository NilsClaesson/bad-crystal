﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroyOnLoad : MonoBehaviour
{
    public string sceneToDestroyAt;
    Scene scene;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        scene = SceneManager.GetActiveScene();
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == sceneToDestroyAt)
        {
            Destroy(transform.gameObject.gameObject);
        }
    }
}
