﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInfo : MonoBehaviour
{
    public static PlayerInfo PI;
    [HideInInspector]
    public int mySelectedCharacter;
    public GameObject[] allCharacters;

    [HideInInspector]
    public int mySelectedClass;
    public GameObject[] allClasses;

    private void OnEnable()
    {
        if (PlayerInfo.PI == null)
        {
            PlayerInfo.PI = this;
        }
        else
        {
            if (PlayerInfo.PI != this)
            {
                Destroy(PlayerInfo.PI.gameObject);
                PlayerInfo.PI = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        if (PlayerPrefs.HasKey("MyCharacter"))
        {
            mySelectedCharacter = PlayerPrefs.GetInt("MyCharacter");
        }
        else
        {
            mySelectedCharacter = 0;
            PlayerPrefs.SetInt("MyCharacter", mySelectedCharacter);
        }

        if (PlayerPrefs.HasKey("MyClass"))
        {
            mySelectedClass = PlayerPrefs.GetInt("MyClass");
        }
        else
        {
            mySelectedClass = 0;
            PlayerPrefs.SetInt("MyClass", mySelectedClass);
        }
    }
}
