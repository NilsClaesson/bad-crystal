﻿using UnityEngine;
using System.Collections.Generic;

public class EmissionFlicker : MonoBehaviour
{

    public float minIntensity = 0f;
    public float maxIntensity = 1f;

    Material m_Material;
    Material startMaterial;
    Color baseColor = new Color(255, 193, 104);

    [Range(1, 200)]
    public int smoothing = 5;

    Queue<float> smoothQueue;
    float lastSum = 0;

    public void Reset()
    {
        smoothQueue.Clear();
        lastSum = 0;
    }

    void Start()
    {
        smoothQueue = new Queue<float>(smoothing);
        m_Material = GetComponent<Renderer>().material;
        startMaterial = m_Material;
    }

    void Update()
    {
        if (m_Material == null)
            return;

        while (smoothQueue.Count >= smoothing)
        {
            lastSum -= smoothQueue.Dequeue();
        }

        float newVal = Random.Range(minIntensity, maxIntensity);
        smoothQueue.Enqueue(newVal);
        lastSum += newVal;

        float intensityFactor = lastSum / (float)smoothQueue.Count;
        baseColor = new Color(255, 193, 104);
        baseColor = baseColor * intensityFactor;
        m_Material.SetColor("_EmissionColor", baseColor);
    }
}