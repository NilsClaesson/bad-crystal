﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faceCamera : MonoBehaviour
{
    Quaternion rotation;
    void start()
    {
        rotation = transform.rotation;
    }

    void LateUpdate()
    {
        transform.LookAt(Camera.main.transform);
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }
}
