﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;

public class MovingCrystal : MonoBehaviour
{

    private PhotonView PV;
    public NavMeshAgent agent;
    public Transform team1Base;
    public Transform team2Base;
    public GameObject[] players;

    private Shader shaderNoOutline;
    private Shader shaderOutline;

    private int controlledBy;
    public float controlDistance = 10f;
    private bool scored;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        players = GameObject.FindGameObjectsWithTag("Avatar");
        shaderOutline = Shader.Find("UniformOutline");
    }

    void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.PlayerList.Length != players.Length)
            {
                players = GameObject.FindGameObjectsWithTag("Avatar");
            }

            if (controlledBy != 0)
            {
                if (ControllerWithinDistance() == false)
                {
                    SetController();
                    SetDestination();
                }
            }
            else if (controlledBy == 0)
            {
                SetController();
                if (controlledBy != 0)
                {
                    SetDestination();
                }
            }

            if (players.Length == 0)
            {
                players = GameObject.FindGameObjectsWithTag("Avatar");
            }

            if (controlledBy != 0)
            {
                CheckGoal();
            }
        }
    }

    bool ControllerWithinDistance()
    {
        Transform bestTarget = transform;
        Vector3 currentPosition = transform.position;
        bool isWithinDistance = false;

        foreach (GameObject potentialTarget in players)
        {
            if (potentialTarget.GetComponent<AvatarRespawn>().playerIsDead == false && (int)potentialTarget.GetComponent<PhotonView>().Owner.CustomProperties["team"] == controlledBy)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < controlDistance)
                {
                    isWithinDistance = true;
                    break;
                }
            }
        }

        return isWithinDistance;
    }

    void SetController()
    {
        Transform bestTarget = transform;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        controlledBy = 0;

        foreach (GameObject potentialTarget in players)
        {
            if (potentialTarget.GetComponent<AvatarRespawn>().playerIsDead == false)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr && dSqrToTarget < controlDistance)
                {
                    closestDistanceSqr = dSqrToTarget;
                    controlledBy = (int)potentialTarget.GetComponent<PhotonView>().Owner.CustomProperties["team"];
                }
            }
        }

    }

    void SetDestination()
    {
        if (controlledBy == 0)
        {
            agent.SetDestination(transform.position);
        }
        if (controlledBy == 1)
        {
            agent.SetDestination(team1Base.position);
        }
        if (controlledBy == 2)
        {
            agent.SetDestination(team2Base.position);
        }
    }

    void CheckGoal()
    {
        if (controlledBy == 1 && scored == false)
        {
            Vector3 distance = transform.position - team1Base.position;
            if (distance.magnitude < 5)
            {
                scored = true;
                string text = "Team 1 Wins!!";
                PV.RPC("RPC_TeamScore", RpcTarget.All, text);
            }
        }

        if (controlledBy == 2 && scored == false)
        {
            Vector3 distance = transform.position - team2Base.position;
            if (distance.magnitude < 5)
            {
                scored = true;
                string text = "Team 2 Wins!!";
                PV.RPC("RPC_TeamScore", RpcTarget.All, text);
            }
        }
    }

    [PunRPC]
    void RPC_TeamScore(string text)
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().TeamWin(text);
    }
}