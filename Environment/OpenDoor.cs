﻿using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    private GameObject gameManager;

    public int doorNumber;
    public float doorOpenAngle = 90.0f;
    public float openSpeed = 2.0f;
    float openTime = 0;
    bool open = false;

    float defaultRotationAngle;
    float currentRotationAngle;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<GameManager>().doors.Add(transform.gameObject);

        defaultRotationAngle = transform.localEulerAngles.y;
        currentRotationAngle = transform.localEulerAngles.y;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            open = !open;
            currentRotationAngle = transform.localEulerAngles.y;
            openTime = 0;
        }

        if (openTime < 1)
        {
            openTime += Time.deltaTime * openSpeed;
        }

        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, Mathf.LerpAngle(currentRotationAngle, defaultRotationAngle + (open ? doorOpenAngle : 0), openTime), transform.localEulerAngles.z);
    }

    public void Open(bool isOpen)
    {
        open = isOpen;
        currentRotationAngle = transform.localEulerAngles.y;
        openTime = 0;
    }
}