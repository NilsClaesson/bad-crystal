﻿using Photon.Pun;
using UnityEngine;
public class NextScene : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int multiplayerSceneIndex = 0;

    public void LoadNextScene()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(multiplayerSceneIndex);
        }
    }
}