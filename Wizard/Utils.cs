﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Utils : MonoBehaviour
{
    private PhotonView PV;
    private WizardCombat wizardCombat;
    public Canvas spawnerCanvas;
    public Canvas abilitiesCanvas;
    public Text ManaText;
    List<int> orderToMonsters;
    Vector3 position;
    int mode;
    bool isSelecting = false;

    bool spawnerMode = true;
    bool abilitiesMode = false;
    bool speedTotemSelected;
    bool wallSpellSelected;
    bool meleeSpawnerSelected;
    bool rangedSpawnerSelected;
    public int meleeSpawnerNumber;
    public int rangedSpawnerNumber;
    Vector3 lookAt;
    Vector3 mousePosition1;
    private GameObject gameManager;
    public static Camera cam;
    static Texture2D _whiteTexture;
    public static Texture2D WhiteTexture
    {
        get
        {
            if (_whiteTexture == null)
            {
                _whiteTexture = new Texture2D(1, 1);
                _whiteTexture.SetPixel(0, 0, Color.white);
                _whiteTexture.Apply();
            }
            return _whiteTexture;
        }
    }

    void Start()
    {
        PV = GetComponent<PhotonView>();
        wizardCombat = GetComponent<WizardCombat>();
        cam = transform.Find("Wizard Camera").GetComponent<Camera>();
        gameManager = GameObject.Find("GameManager");
        ManaText = GameObject.Find("ManaText").GetComponent<Text>();
        orderToMonsters = new List<int>();
    }

    void Update()
    {
        if (PV.IsMine)
        {
            if (meleeSpawnerNumber + rangedSpawnerNumber <= 0)
            {
                spawnerMode = false;
                abilitiesMode = true;

                spawnerCanvas.gameObject.SetActive(false);
                abilitiesCanvas.gameObject.SetActive(true);
            }
            else
            {
                spawnerMode = true;
                abilitiesMode = false;

                spawnerCanvas.gameObject.SetActive(true);
                abilitiesCanvas.gameObject.SetActive(false);
            }

            UnitMovement();

            if (spawnerMode == true)
            {
                SpawnerUI();
            }

            if (abilitiesMode == true)
            {
                AbilitiesUI();
            }

            ManaText.text = wizardCombat.playerMana.ToString();
        }
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            var rect = Utils.GetScreenRect(mousePosition1, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.05f));
            Utils.DrawScreenRectBorder(rect, 1, new Color(0.8f, 0.8f, 0.95f));

            if (gameManager.GetComponent<GameManager>().monsters != null)
            {
                foreach (var monster in gameManager.GetComponent<GameManager>().monsters)
                {
                    if (IsWithinSelectionBounds(monster) == true)
                    {
                        monster.GetComponent<MonsterController>().isSelected = true;
                    }
                }
            }
        }
    }

    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = cam;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition1, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    public static void DrawScreenRect(Rect rect, Color color)
    {
        GUI.color = color;
        GUI.DrawTexture(rect, WhiteTexture);
        GUI.color = Color.white;
    }

    public static void DrawScreenRectBorder(Rect rect, float thickness, Color color)
    {
        // Top
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
        // Left
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
        // Right
        Utils.DrawScreenRect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
        // Bottom
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
    }

    public static Rect GetScreenRect(Vector3 screenPosition1, Vector3 screenPosition2)
    {
        screenPosition1.y = Screen.height - screenPosition1.y;
        screenPosition2.y = Screen.height - screenPosition2.y;

        var topLeft = Vector3.Min(screenPosition1, screenPosition2);
        var bottomRight = Vector3.Max(screenPosition1, screenPosition2);

        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public static Bounds GetViewportBounds(Camera camera, Vector3 screenPosition1, Vector3 screenPosition2)
    {
        var v1 = cam.ScreenToViewportPoint(screenPosition1);
        var v2 = cam.ScreenToViewportPoint(screenPosition2);
        var min = Vector3.Min(v1, v2);
        var max = Vector3.Max(v1, v2);
        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;

        var bounds = new Bounds();
        bounds.SetMinMax(min, max);
        return bounds;
    }

    public void UnitMovement()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClearInput();
            RaycastHit hit = GetMousePositionInPlaneOfLauncher();
            if (hit.transform.tag == "Monster")
            {
                if (hit.transform.gameObject.GetComponent<MonsterController>() == null)
                {
                    Debug.Log("MonsterController script not assigned to monster, it's a spawner perhaps?");
                }
                else
                {
                    hit.transform.gameObject.GetComponent<MonsterController>().isSelected = true;
                }
            }

            isSelecting = true;
            mousePosition1 = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isSelecting = false;
        }

        if (Input.GetMouseButtonDown(1))
        {
            mode = 2;
            foreach (var monster in gameManager.GetComponent<GameManager>().monsters)
            {
                if (monster.GetComponent<MonsterController>().isSelected == true)
                {
                    orderToMonsters.Add(monster.GetPhotonView().ViewID);
                    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        position = hit.point;
                    }
                    else
                    {
                        position = monster.transform.position;
                    }
                }
            }

            int[] orderToMonstersArray = orderToMonsters.ToArray();
            PV.RPC("RPC_SendOrders", RpcTarget.All, (object)orderToMonstersArray, position, mode);
            orderToMonsters.Clear();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            mode = 1;
            foreach (var monster in gameManager.GetComponent<GameManager>().monsters)
            {
                if (monster.GetComponent<MonsterController>().isSelected == true)
                {
                    orderToMonsters.Add(monster.GetPhotonView().ViewID);
                }
            }
            int[] orderToMonstersArray = orderToMonsters.ToArray();
            PV.RPC("RPC_SendOrders", RpcTarget.All, (object)orderToMonstersArray, position, mode);
            orderToMonsters.Clear();
        }

    }


    void SpawnerUI()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ClearInput();
            meleeSpawnerSelected = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ClearInput();
            rangedSpawnerSelected = true;
        }

        if (Input.GetMouseButtonDown(1) && meleeSpawnerSelected == true && meleeSpawnerNumber > 0)
        {
            RaycastHit hit = GetMousePositionInPlaneOfLauncher();
            if (hit.transform.tag == "Ground")
            {
                meleeSpawnerNumber--;
                wizardCombat.SpawnMeleeMonsterSpawner(hit.point, Quaternion.identity);
            }
        }

        if (Input.GetMouseButtonDown(1) && rangedSpawnerSelected == true && rangedSpawnerNumber > 0)
        {
            RaycastHit hit = GetMousePositionInPlaneOfLauncher();
            if (hit.transform.tag == "Ground")
            {
                wizardCombat.SpawnRangedMonsterSpawner(hit.point, Quaternion.identity);
                rangedSpawnerNumber--;
            }
        }
    }

    void AbilitiesUI()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ClearInput();
            speedTotemSelected = true;
        }

        if (Input.GetMouseButtonDown(1) && speedTotemSelected == true && wizardCombat.playerMana >= 50)
        {
            RaycastHit hit = GetMousePositionInPlaneOfLauncher();
            if (hit.transform.tag == "Ground")
            {
                wizardCombat.PV.RPC("RPC_SpellSpeedTotem", RpcTarget.All, hit.point);
                wizardCombat.playerMana = wizardCombat.playerMana - 50;
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ClearInput();
            wallSpellSelected = true;
        }

        if (Input.GetMouseButtonDown(1) && wallSpellSelected == true && wizardCombat.playerMana >= 50)
        {
            RaycastHit hit = GetMousePositionInPlaneOfLauncher();
            if (hit.transform.tag == "Ground")
            {
                wizardCombat.SpawnWall(hit.point, Quaternion.identity);
                wizardCombat.playerMana = wizardCombat.playerMana - 50;
            }
        }

    }

    public void SelectMeleeSpawner()
    {
        ClearInput();
        meleeSpawnerSelected = true;
    }

    public void SelectRangedSpawner()
    {
        ClearInput();
        rangedSpawnerSelected = true;
    }

    public void SelectSpeedTotem()
    {
        ClearInput();
        speedTotemSelected = true;
    }

    void ClearInput()
    {
        meleeSpawnerSelected = false;
        rangedSpawnerSelected = false;
        speedTotemSelected = false;
        wallSpellSelected = false;

        foreach (var monster in gameManager.GetComponent<GameManager>().monsters)
        {
            if (monster.GetComponent<MonsterController>() != null)
            {
                monster.GetComponent<MonsterController>().isSelected = false;
            }
        }
    }

    [PunRPC]
    void RPC_SendOrders(int[] orderToMonstersArray, Vector3 position, int mode)
    {
        foreach (int monsterID in orderToMonstersArray)
        {
            foreach (var monster in gameManager.GetComponent<GameManager>().allEnemies)
            {
                if (monster.GetPhotonView().ViewID == monsterID)
                {
                    monster.GetComponent<MonsterController>().agent.ResetPath();
                    monster.GetComponent<MonsterController>().mode = mode;

                    if (mode == 1)
                    {
                        monster.GetComponent<MonsterController>().destination = transform.position;
                    }
                    else
                    {
                        monster.GetComponent<MonsterController>().destination = position;
                    }
                }
            }
        }
    }

    public void SetSpawnerNumbers(int meleeSpawner, int rangedSpawner)
    {
        meleeSpawnerNumber = meleeSpawner;
        rangedSpawnerNumber = rangedSpawner;
    }


    RaycastHit GetMousePositionInPlaneOfLauncher()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            return hit;
        }
        else throw new UnityException("Mouse position ray not intersecting launcher plane");
    }
}