﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedTotem : MonoBehaviour
{
    private IEnumerator coroutine;
    private GameObject gameManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        StartCoroutine(DestroyTotem());
    }

    void Update()
    {
        foreach (var monster in gameManager.GetComponent<GameManager>().monsters)
        {
            if ((transform.position - monster.transform.position).magnitude < 10f)
            {
                monster.GetComponent<MonsterController>().speedTotemAttackSpeed = 0.6f;
                monster.GetComponent<MonsterController>().agent.speed = monster.GetComponent<MonsterController>().originalMoveSpeed * 2f;
            }
            else
            {
                monster.GetComponent<MonsterController>().speedTotemAttackSpeed = 1f;
                monster.GetComponent<MonsterController>().agent.speed = monster.GetComponent<MonsterController>().originalMoveSpeed;
            }
        }
    }

    IEnumerator DestroyTotem()
    {
        yield return new WaitForSeconds(10);
        foreach (var monster in gameManager.GetComponent<GameManager>().monsters) //Otherwise monsters keep their extra speed
        {
            monster.GetComponent<MonsterController>().speedTotemAttackSpeed = 1f;
            monster.GetComponent<MonsterController>().agent.speed = monster.GetComponent<MonsterController>().originalMoveSpeed;
        }
        Destroy(gameObject);
    }
}
