﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class CameraFollowWizard : MonoBehaviour
{
    public GameObject[] players;
    public Camera cam;
    Vector3 camPos;
    int panSpeed = 24;
    float camDistance;
    int zoomSensitivity = 15;

    private void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Wizard");
        foreach (GameObject player in players)
        {
            if (PhotonView.Get(player).IsMine)
            {
                Camera.main.enabled = false;
                camPos = transform.position;
                camDistance = cam.fieldOfView;
                cam.enabled = true;

                Vector3 cameraPosition = new Vector3(3, 20, -30);
                cam.transform.position = cameraPosition;
                cam.transform.Rotate(55, 0, 0);
            }
        }
    }

    private void FixedUpdate()
    {
        players = GameObject.FindGameObjectsWithTag("Wizard");
        foreach (GameObject player in players)
        {
            if (PhotonView.Get(player).IsMine)
            {
                MoveCam();
                camDistance -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
                cam.fieldOfView = camDistance;

                if (Input.GetKey(KeyCode.P))
                {
                    camPos.x = 0;
                    camPos.z = 0;
                    cam.transform.position = camPos;
                }
                break;
            }
        }
    }

    void MoveCam()
    {
        camPos = transform.position;
        if (Input.mousePosition.x > Screen.width - 30 && Input.mousePosition.y > Screen.height - 30) //Top Right
        {
            camPos.x += panSpeed * Time.deltaTime * 0.7f;
            camPos.z += panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.mousePosition.x > Screen.width - 30 && Input.mousePosition.y < 30) //Bottom Right
        {
            camPos.x += panSpeed * Time.deltaTime * 0.7f;
            camPos.z -= panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.mousePosition.x < 30 && Input.mousePosition.y > Screen.height - 30) //Top Left
        {
            camPos.x -= panSpeed * Time.deltaTime * 0.7f;
            camPos.z += panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.mousePosition.x < 30 && Input.mousePosition.y < 30) //Bottom Left
        {
            camPos.x -= panSpeed * Time.deltaTime * 0.7f;
            camPos.z -= panSpeed * Time.deltaTime * 0.7f;
        }
        if (Input.mousePosition.x > Screen.width - 30 || Input.GetKey(KeyCode.D))
        {
            camPos.x += panSpeed * Time.deltaTime;
        }
        else if (Input.mousePosition.x < 30 || Input.GetKey(KeyCode.A))
        {

            camPos.x -= panSpeed * Time.deltaTime;
        }
        else if (Input.mousePosition.y > Screen.height - 30 || Input.GetKey(KeyCode.W))
        {

            camPos.z += panSpeed * Time.deltaTime;
        }
        else if (Input.mousePosition.y < 30 || Input.GetKey(KeyCode.S))
        {

            camPos.z -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W)) //Top Right
        {
            camPos.x += panSpeed * Time.deltaTime * 0.7f;
            camPos.z += panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D)) //Bottom Right
        {
            camPos.x += panSpeed * Time.deltaTime * 0.7f;
            camPos.z -= panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W)) //Top Left
        {
            camPos.x -= panSpeed * Time.deltaTime * 0.7f;
            camPos.z += panSpeed * Time.deltaTime * 0.7f;
        }
        else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)) //Bottom Left
        {
            camPos.x -= panSpeed * Time.deltaTime * 0.7f;
            camPos.z -= panSpeed * Time.deltaTime * 0.7f;
        }
        cam.transform.position = camPos;
    }
}