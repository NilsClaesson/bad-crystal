﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardParticles : MonoBehaviour
{
    public ParticleSystem dashParticles;
    public ParticleSystem deathParticles;
    public ParticleSystem respawnParticles;
}
