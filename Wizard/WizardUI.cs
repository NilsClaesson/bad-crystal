﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class WizardUI : MonoBehaviour
{
    public Text HealthText;
    public Text ManaText;
    public Text PlayerScore;
    private PhotonView PV;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        HealthText = GameObject.Find("HealthText").GetComponent<Text>();
        ManaText = GameObject.Find("ManaText").GetComponent<Text>();
        PlayerScore = GameObject.Find("PlayerScore").GetComponent<Text>();
    }
}
