﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class WallSpell : MonoBehaviour
{
    private PhotonView PV;
    float heightAdded;
    float heightRate = 0.2f;
    Vector3 newPos;
    private IEnumerator coroutine;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        Vector3 startPosition = new Vector3(transform.position.x, transform.position.y - transform.localScale.y, transform.position.z);
        transform.position = startPosition;
        StartCoroutine(RaiseWall());
    }

    void Update()
    {
        if (transform.gameObject.GetComponent<MonsterSetup>().health <= 0)
        {
            PV.RPC("RPC_DestroyWall", RpcTarget.All);
        }
    }

    IEnumerator RaiseWall()
    {
        while (heightAdded < transform.localScale.y)
        {
            newPos = new Vector3(transform.position.x, transform.position.y + heightRate, transform.position.z);
            transform.position = newPos;
            heightAdded = heightAdded + heightRate;
            yield return new WaitForSeconds(0.05f);
        }
    }

    [PunRPC]
    private void RPC_DestroyWall(PhotonMessageInfo info)
    {
        Destroy(gameObject);
    }
}