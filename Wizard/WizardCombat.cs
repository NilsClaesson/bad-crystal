﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class WizardCombat : MonoBehaviour
{
    public PhotonView PV;
    public GameObject meleeMonsterSpawnerPrefab;
    public GameObject rangedMonsterSpawnerPrefab;
    public GameObject wallPrefab;
    public GameObject speedTotemPrefab;
    Vector3 lookAt;
    private Camera cam;

    public int playerMana;
    private IEnumerator coroutine;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        cam = transform.Find("Wizard Camera").GetComponent<Camera>();
        StartCoroutine(RegenerateMana());
    }

    void Update()
    {
        if (!PV.IsMine)
        {
            return;
        }
    }

    public void SpawnMeleeMonsterSpawner(Vector3 position, Quaternion rotation)
    {
        GameObject meleeMonsterSpawner;
        position = new Vector3(position.x, position.y + 0.5f, position.z);
        meleeMonsterSpawner = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "MeleeMonsterSpawner"), position, rotation);
    }

    public void SpawnRangedMonsterSpawner(Vector3 position, Quaternion rotation)
    {
        GameObject rangedMonsterSpawner;
        position = new Vector3(position.x, position.y + 1, position.z);
        rangedMonsterSpawner = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "RangedMonsterSpawner"), position, rotation);
    }

    [PunRPC]
    void RPC_SpellSpeedTotem(Vector3 position, PhotonMessageInfo info)
    {
        GameObject speedTotem;
        position = new Vector3(position.x, position.y + 1, position.z);
        speedTotem = Instantiate(speedTotemPrefab, position, Quaternion.identity) as GameObject;
    }

    public void SpawnWall(Vector3 position, Quaternion rotation)
    {
        GameObject wall;
        position = new Vector3(position.x, position.y + 1, position.z);
        wall = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "WallSpell"), position, rotation);
    }

    IEnumerator RegenerateMana()
    {
        while (true)
        {
            playerMana = Mathf.Clamp(playerMana + 1, -100, 100);
            yield return new WaitForSeconds(0.1f);
        }
    }

    RaycastHit GetMousePositionInPlaneOfLauncher()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            return hit;
        }
        else throw new UnityException("Mouse position ray not intersecting launcher plane");
    }
}
