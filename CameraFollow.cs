﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public GameObject[] players;

    private void Start()
    {
        transform.rotation = Quaternion.identity;
        transform.Rotate(37, 0, 0);
        offset = new Vector3(0, 10, -13);
        players = GameObject.FindGameObjectsWithTag("Avatar");
        foreach (GameObject player in players)
        {
            if (PhotonView.Get(player).IsMine)
            {
                this.target = player.transform;
                break;
            }
        }
    }

    private void FixedUpdate()
    {
        players = GameObject.FindGameObjectsWithTag("Avatar");
        foreach (GameObject player in players)
        {
            if (PhotonView.Get(player).IsMine)
            {
                this.target = player.transform;
                break;
            }
        }
    }

    private void LateUpdate()
    {
        transform.position = target.position + offset;
    }
}