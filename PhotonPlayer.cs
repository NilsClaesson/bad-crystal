﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{
    public PhotonView PV;
    public GameObject myAvatar;
    public GameObject myClass;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        SpawnAvatar();
    }

    public void SpawnAvatar()
    {
        if (PV.IsMine)
        {
            transform.position = pickSpawn().position;
            myAvatar = AddClass(PlayerInfo.PI.mySelectedClass, pickSpawn().position, pickSpawn().rotation);
        }
    }

    public GameObject AddClass(int whichClass, Vector3 pos, Quaternion rot)
    {
        myClass = PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", PlayerInfo.PI.allClasses[whichClass].name), transform.position, transform.rotation);
        return myClass;
    }

    public Transform pickSpawn()
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["team"] == 1)
        {
            int spawnPicker = Random.Range(0, GameSetup.GS.spawnPointsBlue.Length);
            return GameSetup.GS.spawnPointsBlue[spawnPicker];
        }
        else
        {
            int spawnPicker = Random.Range(0, GameSetup.GS.spawnPointsRed.Length);
            return GameSetup.GS.spawnPointsRed[spawnPicker];
        }
    }
}