﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine;

public class GameSetup : MonoBehaviour
{
    public static GameSetup GS;
    public int nextPlayersTeam;
    public Transform[] spawnPointsBlue;
    public Transform[] spawnPointsRed;

    void Start()
    {
        CreatePlayer();
    }

    private void OnEnable()
    {
        if (GameSetup.GS == null)
        {
            GameSetup.GS = this;
        }
    }

    private void CreatePlayer()
    {
        PhotonNetwork.Instantiate(System.IO.Path.Combine("PhotonPrefabs", "PhotonPlayer"), Vector3.zero, Quaternion.identity);
    }
}