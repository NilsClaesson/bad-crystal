﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RogueFog : MonoBehaviour
{
    private ParticleSystem particles;
    private IEnumerator coroutine;
    private float duration = 6f;

    void Start()
    {
        particles = GetComponent<ParticleSystem>();
        coroutine = disableEmitter(duration - 1);
        StartCoroutine(coroutine);
        coroutine = destroyParticles(duration);
        StartCoroutine(coroutine);
    }

    private IEnumerator disableEmitter(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        particles.Stop();
    }

    private IEnumerator destroyParticles(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this);
    }
}