﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplittingShot : MonoBehaviour
{
    public int arrowSpeed = 100;
    int damage = 50;
    Vector3 velocity;
    [HideInInspector]
    public int numberOfSplits;
    public GameObject splittingShotPrefab;
    private GameObject monster;
    public ParticleSystem explosion;

    void Start()
    {
        var monsterCollided = new List<GameObject>();
        var boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;
        Destroy(gameObject, 4);
    }

    void Update()
    {
        transform.position += velocity * arrowSpeed * Time.deltaTime;
        transform.Rotate(10, 20, 5);
    }

    public void InitializeShot(Vector3 originalDirection)
    {
        velocity = originalDirection;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TakeDamage>() != null && monster != other.gameObject && numberOfSplits < 4)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            numberOfSplits++;
            monster = other.gameObject;

            Split(Quaternion.Euler(0, -20, 0), other.gameObject);
            Split(Quaternion.Euler(0, 20, 0), other.gameObject);

            other.gameObject.GetComponent<TakeDamage>().health -= damage;
            ParticleSystem impactExplosion = Instantiate(explosion, transform.position, Quaternion.identity);

            if (other.gameObject.GetComponent<FlashObject>() != null)
            {
                other.gameObject.GetComponent<FlashObject>().Flash();
            }
            Destroy(gameObject, 0.1f);
        }

        else if (other.gameObject.tag == "Wall")
        {
            ParticleSystem impactExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    public void Split(Quaternion rotation, GameObject colliderMonster)
    {
        GameObject splittingShot;

        splittingShot = Instantiate(splittingShotPrefab, transform.position, Quaternion.identity) as GameObject;
        splittingShot.GetComponent<SplittingShot>().InitializeShot((rotation * velocity));

        if (colliderMonster != null)
        {
            splittingShot.GetComponent<SplittingShot>().monster = colliderMonster;
        }
        splittingShot.GetComponent<SplittingShot>().numberOfSplits = numberOfSplits;
    }
}
