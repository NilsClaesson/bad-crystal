﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinAnimatorRogue : MonoBehaviour
{
    public Animator animator;
    private CharacterController myCC;
    private IEnumerator coroutine;

    void Start()
    {
        animator = GetComponent<Animator>();
        myCC = GetComponentInParent<CharacterController>();
    }

    void Update()
    {
        if (animator.GetBool("isAttacking") == false && animator.GetBool("isDead") == false)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
            {
                animator.SetInteger("condition", 1);
            }

            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
            {
                animator.SetInteger("condition", 0);
            }
        }
    }
}