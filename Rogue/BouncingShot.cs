﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingShot : MonoBehaviour
{
    public int arrowSpeed = 100;
    Vector3 velocity;
    private List<GameObject> monsters;

    void Start()
    {
        monsters = new List<GameObject>();
        var capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
        capsuleCollider.isTrigger = true;
        Destroy(gameObject, 3.0f);
    }

    void Update()
    {
        transform.position += velocity * arrowSpeed * Time.deltaTime;
    }

    public void InitializeArrow(Vector3 originalDirection, float lag)
    {
        velocity = originalDirection;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MeleeMonster" && monsters.Contains(other.gameObject) == false)
        {
            other.gameObject.GetComponent<MonsterSetup>().health -= 50;
            monsters.Add(other.gameObject);
        }
    }
}
