﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockRifleShrapnel : MonoBehaviour
{
    public int arrowSpeed = 100;
    int damage = 50;
    Vector3 velocity;
    public ParticleSystem explosion;

    void Start()
    {
        var boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;
        Destroy(gameObject, 0.5f);
    }

    void Update()
    {
        transform.position += velocity * arrowSpeed * Time.deltaTime;
        transform.Rotate(10, 20, 5);
    }

    public void InitializeShot(Vector3 originalDirection)
    {
        velocity = originalDirection;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Bullet" && other.gameObject.tag != "Ground")
        {
            if (other.gameObject.GetComponent<TakeDamage>() != null)
            {
                other.gameObject.GetComponent<TakeDamage>().health -= damage;
                ParticleSystem impactExplosion = Instantiate(explosion, transform.position, Quaternion.identity);

                if (other.gameObject.GetComponent<FlashObject>() != null)
                {
                    other.gameObject.GetComponent<FlashObject>().Flash();
                }
                Destroy(gameObject);
            }
            else if (other.gameObject.tag != "MarshalSword")
            {
                ParticleSystem arrowExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}