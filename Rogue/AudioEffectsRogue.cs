﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEffectsRogue : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip laserShot1;
    public AudioClip laserShot2;
    public AudioClip laserShot3;
    public AudioClip splittingShot;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayLaserShot()
    {
        int randomNumber = Random.Range(1, 3);

        if (randomNumber == 1)
        {
            audioSource.PlayOneShot(laserShot1, 0.7F);
        }
        if (randomNumber == 2)
        {
            audioSource.PlayOneShot(laserShot2, 0.7F);
        }
        if (randomNumber == 3)
        {
            audioSource.PlayOneShot(laserShot3, 0.7F);
        }
    }

    public void PlaySplittingShot()
    {
        audioSource.PlayOneShot(splittingShot, 0.4F);
    }
}
