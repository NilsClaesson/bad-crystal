﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockRifleShot : MonoBehaviour
{
    public int arrowSpeed = 18;
    Vector3 velocity;
    public GameObject shrapnelPrefab;
    public ParticleSystem explosion;

    void Start()
    {
        var boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;
        Destroy(gameObject, 4);
    }

    void Update()
    {
        transform.position += velocity * arrowSpeed * Time.deltaTime;
        transform.Rotate(10, 20, 5);
    }

    public void InitializeShot(Vector3 originalDirection)
    {
        velocity = originalDirection;
    }

    public void Explode()
    {
        Split(Quaternion.Euler(0, Random.Range(0, 60), 0));
        Split(Quaternion.Euler(0, Random.Range(60, 120), 0));
        Split(Quaternion.Euler(0, Random.Range(120, 180), 0));
        Split(Quaternion.Euler(0, Random.Range(180, 240), 0));
        Split(Quaternion.Euler(0, Random.Range(240, 300), 0));
        Split(Quaternion.Euler(0, Random.Range(300, 360), 0));
        Destroy(gameObject);
    }

    public void Split(Quaternion rotation)
    {
        GameObject shrapnel;
        shrapnel = Instantiate(shrapnelPrefab, transform.position, Quaternion.identity) as GameObject;
        shrapnel.GetComponent<ShockRifleShrapnel>().InitializeShot((rotation * velocity));
    }
}