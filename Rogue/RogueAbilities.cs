﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class RogueAbilities : MonoBehaviour
{
    private PhotonView PV;
    private AvatarSetup avatarSetup;
    private PlayerMovement playerMovement;
    private TakeDamage takeDamage;
    private MonsterSetup monsterSetup;
    private GameObject gameManager;
    private AudioEffectsRogue audioEffectsRogue;
    private Animator animator;
    private CharacterController myCC;
    private ParticleSystem dashParticles;
    private Collider playerCollider;

    [HideInInspector]
    public Transform rayOrigin;
    public GameObject meleeMonster;
    public GameObject BulletPrefab;
    public GameObject piercingArrowPrefab;
    public GameObject splittingShotPrefab;
    public GameObject shockRifleShotPrefab;
    public GameObject rogueFogPrefab;
    public GameObject rangedMonsterProjectile;
    public GameObject MeleeMonsterPrefab;
    public GameObject jetpackParticles;
    public ParticleSystem respawnParticles2;

    private int activeSpell = 1;
    GameObject shockRifleShot;
    bool shockRifleDetonated;
    private float dashSpeed = 0f;
    private bool isJetpacking;
    private IEnumerator coroutine;
    public float fireRate = 1f;
    private float nextFire;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        avatarSetup = GetComponent<AvatarSetup>();
        playerMovement = GetComponent<PlayerMovement>();
        takeDamage = GetComponent<TakeDamage>();
        audioEffectsRogue = GetComponent<AudioEffectsRogue>();
        playerCollider = GetComponent<Collider>();
        monsterSetup = meleeMonster.GetComponent<MonsterSetup>();
        gameManager = GameObject.Find("GameManager");

        myCC = GetComponent<CharacterController>();

    }

    void Update()
    {
        if (animator == null)
        {
            animator = GetComponentInChildren<Animator>();
        }

        if (transform.gameObject.GetComponent<AvatarRespawn>().playerIsDead)
        {
            animator.SetInteger("condition", 4);
            animator.SetBool("isDead", true);
            return;
        }

        if (isJetpacking == false)
        {
            jetpackParticles.SetActive(false);
        }

        if (!PV.IsMine)
        {
            return;
        }

        if (myCC.isGrounded && isJetpacking == true)
        {
            PV.RPC("RPC_NotJetpacking", RpcTarget.All);
        }

        PlayerInput();
    }

    void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            activeSpell = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            activeSpell = 2;
        }

        if (Input.GetMouseButtonDown(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            PV.RPC("RPC_Fire", RpcTarget.All, transform.position, transform.rotation);
        }

        if (Input.GetKey(KeyCode.Space) && transform.gameObject.GetComponent<AvatarSetup>().playerMana > 0)
        {
            Jetpack();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (shockRifleShot != null)
            {
                PV.RPC("RPC_ShockRifleExplosion", RpcTarget.All);
            }

            else if (transform.gameObject.GetComponent<AvatarSetup>().playerMana >= 50)
            {
                transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - 50;
                PV.RPC("RPC_ShockRifle", RpcTarget.All, transform.position, transform.rotation);
            }
        }

        if (Input.GetKeyDown(KeyCode.E) && transform.gameObject.GetComponent<AvatarSetup>().playerMana >= 50)
        {
            transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - 50;
            PV.RPC("RPC_RogueFog", RpcTarget.All, transform.position);
        }

    }

    [PunRPC]
    public void RPC_Fire(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        audioEffectsRogue.PlayLaserShot();
        animator.SetBool("isAttacking", true);
        animator.SetInteger("condition", 2);

        coroutine = setIdle(0.6f);
        StartCoroutine(coroutine);

        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject bullet;

        Vector3 plusY = new Vector3(0, 1, 0);
        Vector3 fireFrom = transform.position + transform.gameObject.GetComponent<PlayerMovement>().lookAt.normalized * 0.0f + plusY; //0.7

        bullet = Instantiate(BulletPrefab, fireFrom, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().InitializeBullet(transform.gameObject.GetInstanceID(), (rotation * Vector3.forward), Mathf.Abs(lag));
    }

    private IEnumerator setIdle(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        animator.SetInteger("condition", 0);
        animator.SetBool("isAttacking", false);
    }

    [PunRPC]
    public void RPC_PiercingArrow(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject piercingArrow;

        Vector3 plusY = new Vector3(0, 0.5f, 0);
        Vector3 fireFrom = transform.position + transform.gameObject.GetComponent<PlayerMovement>().lookAt.normalized * 1.5f + plusY;

        piercingArrow = Instantiate(piercingArrowPrefab, fireFrom, Quaternion.identity) as GameObject;
        piercingArrow.GetComponent<BouncingShot>().InitializeArrow((rotation * Vector3.forward), Mathf.Abs(lag));
    }

    [PunRPC]
    public void RPC_SplittingShot(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        audioEffectsRogue.PlaySplittingShot();
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject splittingShot;

        Vector3 plusY = new Vector3(0, 0.5f, 0);
        Vector3 fireFrom = transform.position + transform.gameObject.GetComponent<PlayerMovement>().lookAt.normalized * 1.5f + plusY;

        splittingShot = Instantiate(splittingShotPrefab, fireFrom, Quaternion.identity) as GameObject;
        splittingShot.GetComponent<SplittingShot>().InitializeShot((rotation * Vector3.forward));
    }

    [PunRPC]
    public void RPC_ShockRifle(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        audioEffectsRogue.PlaySplittingShot();
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);

        Vector3 plusY = new Vector3(0, 0.5f, 0);
        Vector3 fireFrom = transform.position + transform.gameObject.GetComponent<PlayerMovement>().lookAt.normalized * 1.5f + plusY;

        shockRifleShot = Instantiate(shockRifleShotPrefab, fireFrom, Quaternion.identity) as GameObject;
        shockRifleShot.GetComponent<ShockRifleShot>().InitializeShot((rotation * Vector3.forward));
    }

    [PunRPC]
    public void RPC_ShockRifleExplosion(PhotonMessageInfo info)
    {
        shockRifleShot.GetComponent<ShockRifleShot>().Explode();
    }

    [PunRPC]
    public void RPC_RogueFog(Vector3 position, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject rogueFog;

        Vector3 plusY = new Vector3(0, 0.5f, 0);
        Vector3 fireFrom = transform.position + plusY;

        rogueFog = Instantiate(rogueFogPrefab, fireFrom, Quaternion.identity) as GameObject;
    }

    void Jetpack()
    {
        if (isJetpacking == false)
        {
            PV.RPC("RPC_IsJetpacking", RpcTarget.All);
            transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - 15;
        }
        dashSpeed = 30f;
        transform.gameObject.GetComponent<AvatarSetup>().playerMana = transform.gameObject.GetComponent<AvatarSetup>().playerMana - 2;
        myCC.Move((transform.forward + transform.up + transform.up).normalized * Time.deltaTime * dashSpeed);
    }

    [PunRPC]
    public void RPC_IsJetpacking(PhotonMessageInfo info)
    {
        isJetpacking = true;
        jetpackParticles.SetActive(true);
        animator.SetInteger("condition", 3);
    }

    [PunRPC]
    public void RPC_NotJetpacking(PhotonMessageInfo info)
    {
        isJetpacking = false;
        jetpackParticles.SetActive(false);
        animator.SetInteger("condition", 0);
    }
}


